#!make
include .env
export $(shell sed 's/=.*//' .env)
CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

all: install

install:
	$(info Get source code...)
	git clone $(GIT_LINK) $(WORK_DIR)/$(GIT_PROJECT)
	$(info Create image...)
	docker-compose build
	$(info Launch lab...)
	docker-compose up -d


clean:
	$(info Delete lab)
	docker-compose down
	$(info Delete workdir)
	rm -rf $(WORK_DIR)
	$(info Delete lab)
	docker rmi terraform-proxmox:${TERRAFORM_VERSION} --force

shell:
	docker-compose exec terraform /bin/sh
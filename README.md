# Terraform Proxmox Sandbox

This project will create a docker container with:
- terraform
- proxmox plugin configured
- git
- openssh

## Usage

Clone this project

```Shell
git clone https://gitlab.com/terraform36/sandbox-terraform-proxmox.git
cd sandbox-terraform-proxmox
```

Modify the `.env` file if needed

| VARS              | DESCRIPITION                                                                                                | DEFAULT VALUES                                        |
| ----------------- | ----------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- |
| TERRAFORM_VERSION | The terraform version you want to install                                                                   | 0.13.5                                                |
| WORK_DIR          | The name of your workdirectory (who will be created in the following directory and monted in the container) | ./code                                                |
| GIT_PROJECT       | The terraform project folder (Use to auto-deploy via make in the future)                                    |
| GIT_LINK          | The git project you want use                                                                                | git@gitlab.com:network-safer/kubernetes-terraform.git |

Launch this command to create container, clone git project inside `$WORK_DIR` directory and launch container

```Shell
sudo make
```

Modify your terraform project inside the `$WORK_DIR` directory or directly inside the container

Launch a terminal inside the container with the command:

```Shell
sudo make shell
```

If you want purge env, juste execute

```Shell
sudo make clean
```

THe make download and let you use this project , follow the README of the project
[PROJECT TF-PROXMOX-TEMPLATER](https://gitlab.com/terraform36/tf-proxmox-templater/)

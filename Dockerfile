FROM alpine

ARG TERRAFORM_VERSION=0.13.5

RUN apk add --no-cache --virtual .build-deps git openssh bash gcc musl-dev openssl go make

RUN echo "INSTALL TERRAFORM VERSION: ${TERRAFORM_VERSION}" && \
    wget "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"  && \
    unzip "terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && rm "terraform_${TERRAFORM_VERSION}_linux_amd64.zip"  && \
    mv terraform /usr/bin/terraform

WORKDIR /tmp

RUN git clone https://github.com/danitso/terraform-provider-proxmox.git && \
    cd terraform-provider-proxmox && \
    make init && \
    make build  && \
    mkdir -p ~/.terraform.d/plugins/registry.terraform.io/hashicorp/proxmox/1.0.0/linux_amd64  && \
    cp bin/terraform-provider-proxmox_v0.4.0-custom_x4 ~/.terraform.d/plugins/registry.terraform.io/hashicorp/proxmox/1.0.0/linux_amd64/

RUN mkdir /workdir

WORKDIR /workdir

VOLUME ["/workdir"]

CMD [ "/bin/sh" ]